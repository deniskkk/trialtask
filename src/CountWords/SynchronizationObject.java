package CountWords;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class SynchronizationObject implements ApplicationContextAware{
    public Element[] wordsArray;
    private String word;
    private int i, threadsFinishedCounter, threadCount;
    private OutputThread outputThread1, outputThread2, outputThread3, outputThread4;
    private ApplicationContext context;

    public SynchronizationObject() {
        wordsArray = new Element[10000000];
    }
    
    public void init(int threadCount){
        this.threadCount = threadCount;
    }
    
    public synchronized void addWord(String wordPar){
        word = wordPar.toLowerCase();
        i = 0;
        while(wordsArray[i] != null){
            if(wordsArray[i].word.compareTo(word) == 0){
             wordsArray[i].count++;
             break;
            }
            i++;
        }
        if(wordsArray[i] == null){
            wordsArray[i] = new Element(word,1);
        }
    }
    
    public void threadsFinished(){
        threadsFinishedCounter++;
        if (threadsFinishedCounter == threadCount){
            outputThread1 = (OutputThread) context.getBean("outputThread1");
            outputThread2 = (OutputThread) context.getBean("outputThread2");
            outputThread3 = (OutputThread) context.getBean("outputThread3");
            outputThread4 = (OutputThread) context.getBean("outputThread4");
            outputThread1.start();
            outputThread2.start();
            outputThread3.start();
            outputThread4.start();
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext ac) throws BeansException {
        this.context = ac;
    }
}
