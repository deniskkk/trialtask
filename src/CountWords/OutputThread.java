
package CountWords;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class OutputThread extends Thread{
    
    private final int threadId;
    private final SynchronizationObject synchronizationObject;
    private final String outputFileName;
    private final char startingLetter, endingLetter;
    private int i = 0;
    
    public OutputThread(int threadId, 
            SynchronizationObject synchronizationObject, 
            String outputFileName, 
            char startingLetter, char endingLetter){
        
        this.threadId = threadId;
        this.outputFileName = outputFileName;
        this.synchronizationObject = synchronizationObject;
        this.startingLetter = startingLetter;
        this.endingLetter = endingLetter;
    }
    
    @Override
    public void run(){
        try (PrintWriter outputFile = new PrintWriter(new FileWriter(outputFileName))){
            while(synchronizationObject.wordsArray[i] != null) {
                if((synchronizationObject.wordsArray[i].word.charAt(0) >= 
                    startingLetter) && 
                    (synchronizationObject.wordsArray[i].word.charAt(0) <= 
                        endingLetter))
               outputFile.println(synchronizationObject.wordsArray[i].word + " : "
                       + synchronizationObject.wordsArray[i].count);
                
                i++;
            }
        }catch(IOException e){
        }
    }
    
}
