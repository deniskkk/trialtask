package CountWords;

public class Element {
    public String word;
    public int count;
    
    public Element(String word, int count){
        this.word = word;
        this.count = count;
    }
}
