/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CountWords;

import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author Denis
 */
public class InputThread extends Thread {

    private int threadId;
    private final SynchronizationObject synchronizationObject;
    private String inputFileName;
    private int readInt;
    private char character;
    private final String pattern = "abcdefghijklmnopqrstuvwxyzABCDEFGHIGKLMNOPQRSTUVWXYZ'-";
    private String readWord = "";

    public InputThread(SynchronizationObject synchronizationObject) {
        this.synchronizationObject = synchronizationObject;
    }

    public void init(int threadId, String inputFileName) {
        this.inputFileName = inputFileName;
        this.threadId = threadId;
    }

    @Override
    public void run() {
        try (FileReader inputFile = new FileReader(inputFileName)) {
            do {
                readInt = inputFile.read();
                if (readInt != -1) {
                    character = (char) readInt;
                    if (pattern.indexOf(readInt) >= 0) {
                        readWord = readWord + character;
                    } else {
                        if (readWord.compareTo("") != 0) {
                            synchronizationObject.addWord(readWord);
                            readWord = "";
                        }
                    }
                } else {
                    if (readWord.compareTo("") != 0) {
                        synchronizationObject.addWord(readWord);
                        readWord = "";
                    }
                }
            } while (readInt != -1);
        } catch (IOException e) {
        }
        synchronizationObject.threadsFinished();
    }

}
