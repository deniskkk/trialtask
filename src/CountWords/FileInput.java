package CountWords;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class FileInput implements ApplicationContextAware{

    private String[] fileList;
    private ApplicationContext context;
    private InputThread[] inputThreadArray;
    private final SynchronizationObject synchronizationObject;

    public FileInput(SynchronizationObject synchronizationObject) {
        this.synchronizationObject = synchronizationObject;
    }
    
    public void init(String[] fileList){
        
        this.fileList = fileList;
        inputThreadArray = new InputThread[fileList.length];
        synchronizationObject.init(fileList.length);
    }
    
    public void readFiles(){
        for(int i = 0; i < fileList.length; i++){
            inputThreadArray[i] = (InputThread) context.getBean("inputThread");
            inputThreadArray[i].init(i,fileList[i]);
            inputThreadArray[i].start();
        }
            
    }

    @Override
    public void setApplicationContext(ApplicationContext ac) throws BeansException {
        this.context = ac;
    }
    
}
