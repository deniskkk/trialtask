import CountWords.FileInput;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

    public Main() {
    }

    public static void main(String[] args) {
        if(args.length == 0) {
            System.out.println("Programos paleidimo parametrai: failų vardai per tarpą");
            return;
        }
        AbstractApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring.xml");
        applicationContext.registerShutdownHook();
        FileInput fileInput = (FileInput) applicationContext.getBean("FileInput");
        fileInput.init(args);
        fileInput.readFiles();
    }
}
